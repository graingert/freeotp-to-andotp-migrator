FreeOTP to andOTP migrator
==========================

I recently discovered the fantastic [andOTP android TOTP
app](https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp), and promptly installed it.  It does
everything I want, including GPG-encrypted backups, so I wanted to migrate my codes from the unmaintained FreeOTP to it.
Unfortunately, there's no built-in migration function, so I wrote a short script to do that.

Using it is easy, just get your `tokens.xml` file from FreeOTP, usually by using adb:

```
adb pull /data/data/org.fedorahosted.freeotp/shared_prefs/tokens.xml .
```

Then, run the provided script with `./freeotp_migrate.py tokens.xml`.
Save the output in a JSON file, copy it to your phone and select "Restore plaintext JSON backup" in andOTP, and
all your codes will appear.
